function ret = solve_with_LU(A,b)
  n = size(A)(1);
  [L,U,P] = lu(A); # P*A = L*U, P is a permutation matrix
  bb = P*b;
  ys = get_y_with_Lb(L,bb);
  ret = get_x_with_Ub(U, ys);
#  ret = pinv(A)*b; # todo: remove
endfunction
