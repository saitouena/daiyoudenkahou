for N = 4*[1:18] # 10 -> 100 argmin diff = i=4*18
  # set xs
  n = N/4;
  d1 = 2/(n+1);
  xs = zeros(N, 2);
  for i=1:n # x=1
    xs(i, 1) = 1.0;
    xs(i, 2) = -1+d1*i;
  end
  for i=1:n # x=-1
    xs(i+n, 1) = -1.0;
    xs(i+n, 2) = -1+d1*i;
  end
  for i=1:n # y=1
    xs(i+2*n, 1) = -1+d1*i;
    xs(i+2*n, 2) = 1;
  end
  for i=1:n # y=-1
    xs(i+3*n, 1) = -1+d1*i;
    xs(i+3*n, 2) = -1;
  end
  # set xis
  xis = zeros(N,2);
  d2 = 4/(n+1);
  for i=1:n # x=2
    xis(i, 1) = 2.0;
    xis(i, 2) = -2+d2*i;
  end
  for i=1:n # x=-2
    xis(i+n, 1) = -2.0;
    xis(i+n, 2) = -2+d2*i;
  end
  for i=1:n # y=2
    xis(i+2*n, 1) = -2+d2*i;
    xis(i+2*n, 2) = 2;
  end
  for i=1:n # y=-2
    xis(i+3*n, 1) = -2+d2*i;
    xis(i+3*n, 2) = -2;
  end
  # set A
  A = zeros(N,N);
  for i=1:N
    for j=1:N
      A(i,j) = K(xs(i,:), xis(j,:));
    end
  end
  # set vs
  vs = zeros(N,1);
  for i=1:N
    if i>=1&&i<=n
      vs(i) = 1;
    else
      vs(i) = 0;
    end
  end
  qs = solve_with_LU(A,vs);
  testp = [-1,-1];
  value = get_v(qs, testp, xis);
  diff = abs(value-0);
  printf("n=%d, diff=%f\n", N, diff);
  fflush(stdout);
end
sz = 20;
xs = -1+(1/sz)*[0:(2*sz)];
ys = -1+(1/sz)*[0:(2*sz)];
zs = zeros(2*sz+1,2*sz+1);
for i=1:(2*sz+1)
  display(i); fflush(stdout);
  for j=1:(2*sz+1)
    zs(i,j) = get_v(qs, [xs(i), ys(j)], xis);
  end
end
figure
mesh(xs,ys,zs);
