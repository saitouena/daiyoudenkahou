function xs = get_x_with_Ub(U,b)
  n = size(b,1);
  xs = zeros(n,1);
  for _i=1:n
    i = n-_i+1;
    xs(i) = b(i);
    for j=(i+1):n
      xs(i) -= U(i,j)*xs(j);
    end
    xs(i)=xs(i)/U(i,i);
  end
endfunction
