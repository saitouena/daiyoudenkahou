function ys = get_y_with_Lb(L, b)
  n = size(b,1);
  ys = zeros(n,1);
  for i=1:n
    ys(i) = b(i);
    for j=1:(i-1)
      ys(i) -= L(i,j)*ys(j);
    end
  end
end
