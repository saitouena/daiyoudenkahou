rho = 1.0;
R = 2.0;
for N = 1:22 # 22 enough (diff=0)
  theta = 2*pi/N;
  A = zeros(N,N);
  xis = zeros(N,2);
  for j=0:(N-1)
    xis(j+1, 1) = R*cos(2*pi*j/N);
    xis(j+1, 2) = R*sin(2*pi*j/N);
  end
  xs = zeros(N,2);
  for j=0:(N-1)
    xs(j+1,1) = rho*cos(2*pi*j/N);
    xs(j+1,2) = rho*sin(2*pi*j/N);
  end
  for i=1:N
    for j=1:N
      A(i, j) = K(xs(i,:),xis(j,:));
    end
  end
  vs = zeros(N,1);
  for i=1:N
    vs(i) = f(xs(i,1), xs(i,2));
  end
  qs = solve_with_LU(A, vs); # todo ここをLU分解でとくようにする
  testp = rho*[cos(pi/N),sin(pi/N)];
  value = get_v(qs, testp, xis);
  diff = abs(value-f(testp(1),testp(2)));
  printf("n=%d, diff=%f\n", N, diff);
  fflush(stdout);
end
# plot
sz = 50;
xs = -1+(1/sz)*[0:(2*sz)];
ys = -1+(1/sz)*[0:(2*sz)];
zs = zeros(2*sz+1,2*sz+1);
for i=1:(2*sz+1)
  display(i); fflush(stdout);
  for j=1:(2*sz+1)
    if( xs(i)*xs(i)+ys(j)*ys(j)<=rho*rho)
      zs(i,j) = get_v(qs, [xs(i), ys(j)], xis);
    else
      zs(i,j) = NA;
    end
  end
end
figure
mesh(xs,ys,zs);
