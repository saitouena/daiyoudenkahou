function ret = K(x,xi)
  ret = - log(norm(x-xi))/(2*pi);
endfunction
